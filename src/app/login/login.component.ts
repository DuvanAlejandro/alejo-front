import { Component, OnInit } from '@angular/core';
import {LoginService} from './login.service';
import {Router} from '@angular/router';
import {Login,Usuario} from './login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private usuario:Usuario = new Usuario();
  private login:Login = new Login();

  constructor(private loginService:LoginService, private router:Router) { }

  ngOnInit() {

  }



  public logear():void{



      this.loginService.getlogin(this.login).subscribe(
          json=>{
            this.usuario = json.usuario;
            alert(this.usuario.id);
            this.router.navigate(['/clientes', this.usuario.id]);
          }

      )



  }




}
