import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import swal from 'sweetalert2';
import {Router} from '@angular/router';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Login,} from './login';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

    private urlEndPoint: string  = 'http://localhost:9999/api/user/log';
    private httpHeaders = new HttpHeaders({'Content-Type':'application/json'})

  constructor(private http: HttpClient, private route:Router) { }

  getlogin(login:Login):Observable<any>{
    return this.http.post<any>('http://localhost:9999/api/user/log',login,{headers:this.httpHeaders}).pipe(
      catchError( e=>{

        console.error( e.error.message);
        swal.fire('Error de cuenta o contraseña', e.error.mensaje, 'error');
        return throwError(e);
    }));

  }








}
