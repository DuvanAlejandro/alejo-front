export class Login{
  username: String;
  password: String;
}

export class Usuario{
  id:number;
  name:string;
  last_name:string;
  email:string;
  pass:string;
  address:string;
  last_login:string;
}
