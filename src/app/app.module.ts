import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import { DirectivaComponent } from './directiva/directiva.component';
import { ClientesComponent } from './clientes/clientes.component';
import {ClienteService} from './clientes/cliente.service';
import {RouterModule,Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { FormComponent } from './clientes/form.component';
import {FormsModule} from '@angular/forms';
import { LoginComponent } from './login/login.component';
import {LoginService} from './login/login.service';

const routes:Routes = [
  {path:'',redirectTo: 'login',pathMatch:'full'},
  {path:'directivas',component: DirectivaComponent},
  {path:'clientes/:id',component: ClientesComponent},
  {path:'clientes',component: ClientesComponent},
  {path:'clientes/form/:id', component:FormComponent},
  {path:'clientes/form', component:FormComponent},
  {path:'login',component:LoginComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    DirectivaComponent,
    ClientesComponent,
    FormComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [ClienteService,LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
