import { Component, OnInit } from '@angular/core';
import { Cliente, Sector, Usuario,Contacto} from './cliente';
import {ClienteService} from './cliente.service';
import { Router ,ActivatedRoute} from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {


  private cliente : Cliente = new Cliente();
  private usuario : Usuario = new Usuario();
  private sector : Sector = new Sector();
  private contacto : Contacto = new Contacto();
  private sectores : Sector[];
  private titulo:string = "Crear Cliente";




  constructor(private clienteService:ClienteService, private router:Router, private activatedRoute : ActivatedRoute) { }

  ngOnInit() {
    this.cargarUsuario();
    this.cargarSectores();
  }

  public cargarUsuario():void{

    this.activatedRoute.params.subscribe( params => {
       let id = params["id"]
       if(id){
         this.clienteService.getUsuario(id).subscribe(
           json => {this.usuario = json.usuario}
         )
       }
    })
  }

  public cargarSectores(){
    this.clienteService.getSector().subscribe(
      json => this.sectores = json.sectores

    );
  }

  public crear():void{


    this.cliente.usuario = this.usuario;
    this.cliente.sector = this.sector;
    this.contacto.cliente = this.cliente;
    alert(this.contacto.cliente.name);

    this.clienteService.createClientes(this.cliente).subscribe(response =>{


      swal.fire({
      type: 'success',
      title: `Cliente ${response.cliente.name} creado con exito`,
      showConfirmButton: false,
      timer: 1500
    })


    this.router.navigate(['/clientes',this.usuario.id])
  })
  }
}
