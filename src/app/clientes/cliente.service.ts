import { Injectable } from '@angular/core';
import {Cliente, Contacto} from './cliente';
import {Observable , throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import swal from 'sweetalert2';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class ClienteService {

    private urlEndPoint: string  = 'http://localhost:9999/api/clientes';

  constructor(private http: HttpClient, private route:Router) { }

    getClientes(id): Observable<any>{
    //  return of(CLIENTES);
    return this.http.get<any>(`${'http://localhost:9999/api/clientes'}/${id}`);

    }

    getSector(): Observable<any>{
    //  return of(CLIENTES);
    return this.http.get<any>('http://localhost:9999/api/sector');

    }

    createContact(contacto: Contacto): Observable<any>{


      return this.http.post<any>('http://localhost:9999/api/sector',contacto).pipe(
        catchError( e=>{

          this.route.navigate(['/clientes']);
          console.error( e.error.message);
          swal.fire('Error al editar', e.error.mensaje, 'error');
          return throwError(e);
      }));

    }

    createClientes(cliente:Cliente): Observable<any>{


      return this.http.post<any>('http://localhost:9999/api/clientes',cliente).pipe(
        catchError( e=>{

          this.route.navigate(['/clientes']);
          console.error( e.error.message);
          swal.fire('Error al crear', e.error.mensaje, 'error');
          return throwError(e);
      }));

    }

    getSectorName(name): Observable<any>{

      return this.http.get<any>('http://localhost:9999/api/sector/name',name).pipe(
        catchError( e=>{

          this.route.navigate(['/clientes']);
          console.error( e.error.message);
          swal.fire('Error al editar', e.error.mensaje, 'error');
          return throwError(e);
      }));

    }



    getUsuario(id):Observable<any>{

        return this.http.get<any>(`${'http://localhost:9999/api/user'}/${id}`).pipe(
          catchError( e=>{

            this.route.navigate(['/clientes']);
            console.error( e.error.message);
            swal.fire('Error al editar', e.error.mensaje, 'error');
            return throwError(e);
        }));
    }

    editCliente(cliente:Cliente,id):Observable<any>{
      return this.http.put<Cliente>(`${this.urlEndPoint}/${id}`,cliente).pipe(
        catchError( e=>{

          this.route.navigate(['/clientes']);
          console.error( e.error.message);
          swal.fire('Error al editar', e.error.mensaje, 'error');
          return throwError(e);
      }));
    }


    deleteCliente(id) :Observable<Cliente>{
       return this.http.delete<Cliente>(`${this.urlEndPoint}/${id}`).pipe(
         catchError(e =>{
            console.error( e.error.message);
            swal.fire('Error al editar', e.error.mensaje, 'error');
            return throwError(e);
         })
       );
    }






}
