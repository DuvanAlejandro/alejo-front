export class Cliente {
  id: number;//
  name:string;//
  email: string;//
  nit: string;
  address: string;//
  city: string;//
  created_date:Date;//
  sector:Sector;
  usuario:Usuario;

}

export class Sector{

  id: number;
  sector_name:string;

}

export class Usuario{
  id:number;
  name:string;
  last_name:string;
  email:string;
  pass:string;
  address:string;
  last_login:Date;
}


export class Contacto{

  id:number;
	name:string;
	email:string;
  cliente:Cliente;

}
