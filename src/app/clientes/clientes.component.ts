import { Component, OnInit } from '@angular/core';
import {Cliente, Usuario} from './cliente';
import {ClienteService} from './cliente.service';
import {Router,ActivatedRoute} from '@angular/router';
import swal from 'sweetalert2';


@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  clientes: Cliente[];
  usuario: Usuario = new Usuario();

  //cliente:Cliente;

  constructor(private clienteService: ClienteService, private router:Router,private activatedRoute : ActivatedRoute) { }


  ngOnInit() {
     this.cargarClientes();
  }

  public cargarClientes():void{

    this.activatedRoute.params.subscribe( params => {
       let id = params["id"]
       if(id){

         this.clienteService.getUsuario(id).subscribe(
           json =>
           {
             this.usuario = json.usuario
             this.clienteService.getClientes(this.usuario.id).subscribe(
               json => this.clientes = json.clientes

             );
           }

         );
//alert(this.usuario.id)




       }
    })
  }

/*

  public eliminar(cliente:Cliente):void{



      swal.fire({
          title: '¿Estas seguro?',
          text: `Seguro que quieres eliminar a ${cliente.name}`,
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, eliminarlo'
      }).then((result) => {
      if (result.value) {
        this.clienteService.deleteCliente(cliente.id_client).subscribe(res =>{

               this.clienteService.getClientes(this.usuario.id_user).subscribe(
                 clientes => this.clientes = clientes
               )
        })

        swal.fire(

          'Eliminado!',
          'El registro fue elmiminado.',
          'success'
        )


      }
    })

  }*/


}
